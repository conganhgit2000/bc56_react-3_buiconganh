import React, { Component } from "react";
import CartShoe from "./CartShoe";
import ListShoe from "./ListShoe";
import DetailShoe from "./DetailShoe";
import { shoeArr } from "./data";
import { message } from "antd";

export default class Ex_shoe extends Component {
  state = {
    shoeArr: shoeArr,
    detail: shoeArr[0],
    cart: [],
  };
  handleAdd = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => item.id === shoe.id);
    if (index === -1) {
      let newShoe = { ...shoe, soLuong: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].soLuong++;
    }
    this.setState({
      cart: cloneCart,
    });
  };
  handleRemove = (shoeId) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => item.id === shoeId);
    cloneCart.splice(index, 1);
    this.setState({
      cart: cloneCart,
    });
    //thông báo xóa thành công
    message.success("xóa thành công");
  };
  handleViewDetail = (shoe) => {
    this.setState({
      detail: shoe,
    });
  };
  handleChangeQuantity = (id, option) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => item.id === id);
    cloneCart[index].soLuong = cloneCart[index].soLuong + option;
    cloneCart[index].soLuong === 0 && cloneCart.splice(index, 1);
    this.setState({ cart: cloneCart });
  };
  //dùng reduce để tính giá trị của giỏ hàng
  render() {
    return (
      <div>
        <div className="row">
          <div className="col-7">
            <CartShoe
              handleChangeQuantity={this.handleChangeQuantity}
              handleRemove={this.handleRemove}
              cart={this.state.cart}
            />
          </div>
          <div className="col-5">
            <ListShoe
              handleViewDetail={this.handleViewDetail}
              handleAdd={this.handleAdd}
              list={this.state.shoeArr}
            />
          </div>
        </div>
        <DetailShoe detail={this.state.detail} />
      </div>
    );
  }
}
