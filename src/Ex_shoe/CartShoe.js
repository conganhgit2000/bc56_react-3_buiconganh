import React, { Component } from "react";

export default class CartShoe extends Component {
  renderTbody = () => {
    let { cart, handleRemove, handleChangeQuantity } = this.props;
    return cart.map((item) => {
      return (
        <tr>
          <th>${item.name}</th>
          <th>${item.price}</th>
          <th>
            <img width={100} src={item.image} alt="" />
          </th>
          <th>
            <button
              onClick={() => {
                handleChangeQuantity(item.id, -1);
              }}
              className="btn btn-danger"
            >
              -
            </button>
            <strong>{item.soLuong}</strong>
            <button
              onClick={() => {
                handleChangeQuantity(item.id, +1);
              }}
              className="btn btn-danger"
            >
              +
            </button>
          </th>
          <th>
            <button
              onClick={() => {
                handleRemove(item.id);
              }}
              className="btn btn-danger"
            >
              Delete
            </button>
          </th>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Price</th>
            <th>Image</th>
            <th>Quantity</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
      </table>
    );
  }
}
